<?php 

	$username_volunteer = $_SESSION['username_vol'];

	$query_get_avail_event = "SELECT * FROM `events` INNER JOIN organisasi ON events.username_organisasi=organisasi.username WHERE id_event NOT IN (SELECT join_events.id_event FROM join_events INNER JOIN events ON events.id_event=join_events.id_event INNER JOIN organisasi ON events.username_organisasi=organisasi.username WHERE username_volunteer='$username_volunteer')";

	$result = mysqli_query($koneksi,$query_get_avail_event);

 ?>

<br>
	<!--==========================
      More Features Section
    ============================-->
    <section id="more-features" class="section-bg">
      <div class="container">

        <div class="section-header text-center">
          <h3 class="section-title">Join Event
          	</h3>
          <span class="section-divider"></span>
          <small>Pilih event yang kamu mau!</small>

        </div>
        <br>
        <div class="row">

        	<?php 
        	// iterate the data
        	while ($data = mysqli_fetch_assoc($result)) {
        	 ?>
        	<div class="col-lg-4">
        		<div class="card">
				  <img src="./assets/img/events/<?php echo($data['gambar']) ?>" class="card-img-top" style="  width:100%;
    height: 230px;">
				  <div class="card-body">
				    <h5 class="card-title"><?php echo($data['nama_event']) ?> - <small><?php echo($data['nama']) ?></small></h5>
				    <small class="card-text"><?php echo $data['deskripsi_event']; ?> </small>
					<br>
					<ul class="list-unstyled">
					   <li> <i class="ion-ios-stopwatch-outline"></i> <?php echo($data['waktu_event']) ?> </li>
					   <li> <i class="ion-ios-home"></i> <?php echo $data['tempat_event'] ?></li>
					</ul>
					<div class="text-center">
			
					 <a class="btn btn-success text-center " href="volunteer.php?page=join_event&id=<?php echo($data['id_event']) ?>&username=<?php echo($_SESSION['username_vol']) ?>" role="button">Join Event</a>

					</div>

				  </div>
				</div>
        		
        	</div>
        	<?php 

        	} ?>

      
        </div>
      </div>
    </section><!-- #more-features -->

