
<?php 
	// get data
	
	$username = $_SESSION['username_vol'];
	$query_get = "SELECT * FROM volunteer WHERE username='$username'";

	$result = mysqli_query($koneksi,$query_get);

	$data = mysqli_fetch_assoc($result);

 ?>

<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Edit Profile</h3>
              <small> Silahkan edit data Anda!</small>

              <div class="container text-left">
              	<br>
              	<form method="POST" action="model/update_vol.php?username=<?php echo($username) ?>" >
              		<div class="form-group">
					    <label for="nama" >Nama</label>
					    <input type="text" class="form-control" id="nama"  placeholder="Masukkan nama anda!" name="nama" value="<?php echo $data['nama']; ?>" required>
					  </div>
					  <div class="form-group" >
					    <label for="alamat" >Alamat</label>
					    <input type="text" class="form-control" id="alamat"  placeholder="Masukkan alamat anda!" name="alamat" value="<?php echo $data['alamat']; ?>"required>
					  </div>
					  <div class="form-group" >
					    <label for="no_telp" >Nomor Telepon</label>
					    <input type="number" class="form-control" id="no_telp"  placeholder="Masukkan no telepon anda!" name="no_telp" value="<?php echo $data['no_telp']; ?>" required>
					  </div>
					   <div class="form-group" >
					    <label for="biografi" >Biografi</label>
					    <textarea type="text" class="form-control" id="biografi"  rows="4" placeholder="Masukkan biografi anda!" name="biografi" required><?php echo $data['biografi']; ?> </textarea> 
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email anda!" value="<?php echo $data['email']; ?>" name="email" required>
					  </div>
					  <div class="form-group">
					    <label for="passwdord">Password</label>
					    <input type="password" class="form-control" id="passwdord" placeholder="Password" name="password" value="<?php echo $data['password']; ?>" required>
					  </div>
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Edit</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->