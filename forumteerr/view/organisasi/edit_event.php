	<?php 

		$id_event = $_GET['id'];
		$query_get = "SELECT * FROM events WHERE id_event='$id_event'";

		$result = mysqli_query($koneksi,$query_get);

		$data = mysqli_fetch_assoc($result);
	 ?>


	<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
      
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Edit Event</h3>
              <small> Silahkan ubah data event organisasi Anda dengan benar!</small>

              <div class="container text-left">
              	<br>
              	
              	<form method="POST" action="model/update_events.php?id=<?php echo($id_event) ?>" enctype="multipart/form-data">
              		<div class="form-group">
					    <label for="nama" >Nama Event</label>
					    <input type="text" class="form-control" id="nama"  placeholder="Masukkan Nama Event!" name="nama_event" value="<?php echo($data['nama_event']) ?>"required>
					  </div>
					  <div class="form-group" >
					    <label for="deskripsi" >Deskripsi Event</label>
					    <textarea class="form-control" id="deskripsi"  placeholder="Masukkan Deskripsi Event!" name="deskripsi_event" required> <?php echo($data['deskripsi_event']) ?></textarea>
					  </div>
					  <div class="row">
					  	<div class="col-md-6">
					  		<div class="form-group" >
							    <label for="tempat" >Tempat Event</label>
							    <input type="text" class="form-control" id="tempat"  placeholder="Masukkan Tempat Event!" name="tempat_event" value="<?php echo($data['tempat_event']) ?>" required>
							  </div>
					  	</div>
					  	<div class="col-md-6">
					  		 <div class="form-group" >
						    <label for="Waktu" >Waktu Event</label>
						    <input type="date" class="form-control" id="Waktu"  placeholder="Masukkan Tempat Event!" name="waktu_event" value="<?php echo($data['waktu_event']) ?>" required>
						  </div>
					  	</div>
					  </div>
					  <div class="form-group">
					  	<div class="text-center">
					  		<small>gambar lama</small><br>
		              		<img class="img-responsive" src="./assets/img/events/<?php echo($data['gambar']) ?>" style="width:250px ; height:250px ">

		              	</div>

						  <div class="input-group">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
						  </div>
						  <div class="custom-file">
						    <input type="file" class="custom-file-input" id="inputGroupFile01"
						      aria-describedby="inputGroupFileAddon01" name="gambar">
						    <label class="custom-file-label" for="inputGroupFile01">Pilih gambar terbaru!</label>
						  </div>
						</div>
					  </div>
					   
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Update</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->