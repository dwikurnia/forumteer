
<?php 
	// get data organisasi
	
	$username = $_SESSION['username_org'];
	$query_get = "SELECT * FROM organisasi WHERE username='$username'";

	$result = mysqli_query($koneksi,$query_get);

	$data = mysqli_fetch_assoc($result);

 ?>

<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Edit Organisasi</h3>
              <small> Silahkan edit data organisasi Anda!</small>

              <div class="container text-left">
              	<br>
              	<form method="POST" action="model/update_org.php?username=<?php echo($username) ?>" >
              		<div class="form-group">
					    <label for="nama" >Nama Organisasi</label>
					    <input type="text" class="form-control" id="nama"  placeholder="Masukkan nama organisasi anda!" name="nama" value="<?php echo $data['nama']; ?>" required>
					  </div>
					  <div class="form-group" >
					    <label for="alamat" >Alamat Organisasi</label>
					    <input type="text" class="form-control" id="alamat"  placeholder="Masukkan alamat organisasi anda!" name="alamat" value="<?php echo $data['alamat']; ?>"required>
					  </div>
					  <div class="form-group" >
					    <label for="no_telp" >Nomor Telepon Organisasi</label>
					    <input type="number" class="form-control" id="no_telp"  placeholder="Masukkan no telepon organisasi anda!" name="no_telp" value="<?php echo $data['no_telp']; ?>" required>
					  </div>
					   <div class="form-group" >
					    <label for="tentang" >Tentang Organisasi</label>
					    <textarea type="text" class="form-control" id="tentang"  rows="4" placeholder="Masukkan tentang organisasi anda!" name="tentang" required><?php echo $data['tentang']; ?> </textarea> 
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email anda!" value="<?php echo $data['email']; ?>" name="email" required>
					  </div>
					  <div class="form-group">
					    <label for="passwdord">Password</label>
					    <input type="password" class="form-control" id="passwdord" placeholder="Password" name="password" value="<?php echo $data['password']; ?>" required>
					  </div>
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Edit</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->