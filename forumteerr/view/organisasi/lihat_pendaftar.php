<?php 
	// get list event
	$id_event = $_GET['id'];
	$query_get_pendaftar = "SELECT * FROM volunteer INNER JOIN join_events ON join_events.username_volunteer=volunteer.username INNER JOIN events ON events.id_event = join_events.id_event WHERE join_events.id_event='$id_event'";

	$result = mysqli_query($koneksi,$query_get_pendaftar);

 ?>

<br>
	<!--==========================
      More Features Section
    ============================-->
    <section id="more-features" class="section-bg">
      <div class="container">

        <div class="section-header tex">
          <h3 class="section-title">List Pendaftar
          	</h3>
          <span class="section-divider"></span>
          <div class="text-center">
          	<small> List volunteer pada event ini.</small>
          </div>

        </div>
        <br>
        <div class="card">
        	<table class="table">
			  <thead>
			    <tr>
			      <th scope="col">No</th>
			      <th scope="col">Nama</th>
			      <th scope="col">Email</th>
			      <th scope="col">No. Tel</th>
			      <th scope="col">L/P</th>
			      <th scope="col">TTL</th>
			      <th scope="col">Alamat</th>
			      <th scope="col">Biografi</th>
			    </tr>
			  </thead>
			  <tbody>
			  	<?php 
			  		$no = 1;
			  		while ($data = mysqli_fetch_assoc($result)) {
			  			# code...
			  		
			  	 ?>
			    <tr>
			      <td><?php echo $no ?></td>
			      <td><?php echo $data['nama'] ?></td>
			      <td><?php echo $data['email'] ?></td>
			      <td><?php echo $data['no_telp'] ?></td>
			      <td><?php echo $data['jenis_kelamin'] ?></td>
			      <td><?php echo $data['tanggal_lahir'] ?></td>
			      <td><?php echo $data['alamat'] ?></td>
			      <td><?php echo $data['biografi'] ?></td>
			    </tr>
			    <?php 
			    $no++;
			    } ?>
			  </tbody>
			</table>

        </div>
      </div>
    </section><!-- #more-features -->

