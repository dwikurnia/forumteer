<?php 
	// get list event
	$username_organisasi = $_SESSION['username_org'];
	$query_get_events = "SELECT * FROM events WHERE username_organisasi='$username_organisasi'";

	$result = mysqli_query($koneksi,$query_get_events);

 ?>

<br>
	<!--==========================
      More Features Section
    ============================-->
    <section id="more-features" class="section-bg">
      <div class="container">

        <div class="section-header tex">
          <h3 class="section-title">Event Organisasi  
          	</h3>
          <span class="section-divider"></span>
          <div class="text-center">
          	          <a class="btn btn-success text-center " href="organisasi.php?page=create_event" role="button">Buat Event</a>
          </div>

        </div>
        <br>
        <div class="row">

        	<?php 
        	// iterate the data
        	while ($data = mysqli_fetch_assoc($result)) {
        	 ?>
        	<div class="col-lg-4">
        		<div class="card">
				  <img src="./assets/img/events/<?php echo($data['gambar']) ?>" class="card-img-top" style="  width:100%;
    height: 230px;">
				  <div class="card-body">
				    <h5 class="card-title"><?php echo($data['nama_event']) ?></h5>
				    <small class="card-text"><?php echo $data['deskripsi_event']; ?> </small>
					<br>
					<ul class="list-unstyled">
					   <li> <i class="ion-ios-stopwatch-outline"></i> <?php echo($data['waktu_event']) ?> </li>
					   <li> <i class="ion-ios-home"></i> <?php echo $data['tempat_event'] ?></li>
					</ul>
					<div class="text-center">
					<a class="btn btn-default text-center " href="organisasi.php?page=lihat_pendaftar&id=<?php echo($data['id_event']) ?>" role="button">Lihat Pendaftar </a>	
					
					<a class="btn btn-primary text-center " href="organisasi.php?page=edit_event&id=<?php echo($data['id_event']) ?>" role="button">Edit </a>
					<a  onClick="return confirm('Apakah kamu yakin akan delete event ini? ')" class="btn btn-danger text-center " href="organisasi.php?page=delete_event&id=<?php echo($data['id_event']) ?>" role="button">Delete </a>
					</div>

				  </div>
				</div>
        		
        	</div>
        	<?php 

        	} ?>

      
        </div>
      </div>
    </section><!-- #more-features -->

