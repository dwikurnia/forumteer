<?php 
  include('../config/db.php');


$target_dir = "../assets/img/events/";
$target_file = $target_dir . basename($_FILES["gambar"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["gambar"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["gambar"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)) {
        //echo "The file ". basename( $_FILES["gambar"]["name"]). " has been uploaded.";
        $file_gambar = basename( $_FILES["gambar"]["name"]);
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}

if ($uploadOk){
    $username_organisasi = $_GET['username'];
    $nama_event = $_POST['nama_event'];
    $deskripsi_event = $_POST['deskripsi_event'];
    $tempat_event = $_POST['tempat_event'];
    $waktu_event = $_POST['waktu_event'];
    $gambar = $file_gambar;
   

    $insertQuery = "INSERT INTO events (username_organisasi,nama_event,deskripsi_event,tempat_event,waktu_event,gambar) VALUES ('$username_organisasi','$nama_event','$deskripsi_event','$tempat_event','$waktu_event','$gambar')";


    if (mysqli_query($koneksi, $insertQuery)) {
       echo "
          <script> alert('Submit Event Berhasil!') 
          window.location = '../organisasi.php';
          </script>
        ";
    } else {
        echo "Error: " . $insertQuery . "<br>" . mysqli_error($koneksi);
    }
  }
  
 ?>