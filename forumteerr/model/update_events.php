<?php 
  include('../config/db.php');
  $id_event = $_GET['id'];

if(!empty($_FILES['gambar']['name'])){
    $target_dir = "../assets/img/events/";
    $target_file = $target_dir . basename($_FILES["gambar"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    // Check if image file is a actual image or fake image
    if(isset($_POST["submit"])) {
        $check = getimagesize($_FILES["gambar"]["tmp_name"]);
        if($check !== false) {
            echo "File is an image - " . $check["mime"] . ".";
            $uploadOk = 1;
        } else {
            echo "File is not an image.";
            $uploadOk = 0;
        }
    }
    // Check if file already exists
    if (file_exists($target_file)) {
        echo "Sorry, file already exists.";
        $uploadOk = 0;
    }
    // Check file size
    if ($_FILES["gambar"]["size"] > 500000) {
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["gambar"]["tmp_name"], $target_file)) {
            //echo "The file ". basename( $_FILES["gambar"]["name"]). " has been uploaded.";
            $file_gambar = basename( $_FILES["gambar"]["name"]);
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }

    if ($uploadOk){
        $nama_event = $_POST['nama_event'];
        $deskripsi_event = $_POST['deskripsi_event'];
        $tempat_event = $_POST['tempat_event'];
        $waktu_event = $_POST['waktu_event'];
        $gambar = $file_gambar;
       

        $update_query = "UPDATE events SET nama_event='$nama_event', deskripsi_event='$deskripsi_event',tempat_event='$tempat_event',waktu_event='$waktu_event',gambar='$gambar' WHERE id_event='$id_event'";


        if (mysqli_query($koneksi, $update_query)) {
           echo "
              <script> alert('Edit Event Berhasil!') 
              window.location = '../organisasi.php';
              </script>
            ";
        } else {
            echo "Error: " . $update_query . "<br>" . mysqli_error($koneksi);
        }
    }
} else {
    // update without photo
        $nama_event = $_POST['nama_event'];
        $deskripsi_event = $_POST['deskripsi_event'];
        $tempat_event = $_POST['tempat_event'];
        $waktu_event = $_POST['waktu_event'];

        $update_query = "UPDATE events SET nama_event='$nama_event', deskripsi_event='$deskripsi_event',tempat_event='$tempat_event',waktu_event='$waktu_event' WHERE id_event='$id_event'";


        if (mysqli_query($koneksi, $update_query)) {
           echo "
              <script> alert('Edit Event Berhasil!') 
              window.location = '../organisasi.php';
              </script>
            ";
        } else {
            echo "Error: " . $update_query . "<br>" . mysqli_error($koneksi);
        }

}
  
 ?>