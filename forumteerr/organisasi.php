<?php 
// home page untuk organisasi
session_start(); 
// kalo gak ada sesi kemudian dilempar ke login
if ((!$_SESSION['username_org'])){
  echo "<meta http-equiv=refresh content=\"0;url=index.php\">";
  exit;
}

$username = $_SESSION['username_org'];

include 'config/db.php';

include 'layout/header.php';
include 'layout/nav_org.php';

include 'controller/c_organisasi.php';

include 'layout/footer.php';

 ?>