 <section id="intro">

    <div class="intro-text">
      <h2>Selamat datang di Forumteer!</h2>
      <p>Sebuah platform bagi organisasi dan relawan berkumpul!</p>
      <a href="registrasi_vol.php" class="btn-get-started scrollto">Ayo Daftar Volunteer!</a>
      <a href="registrasi_org.php" class="btn-get-started scrollto">Ayo Daftarin Organisasimu!</a>
    </div>

    <div class="product-screens">

      <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
        <img src="./assets/img/product-screen-1.png" alt="">
      </div>

      <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
        <img src="./assets/img/product-screen-2.png" alt="">
      </div>

      <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
        <img src="./assets/img/product-screen-3.png" alt="">
      </div>

    </div>

  </section><!-- #intro -->