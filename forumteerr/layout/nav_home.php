<!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="index.php" class="scrollto">Forumteer</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="index.php">Home</a></li>
          <li><a href="about_us.php">Tentang Kami</a></li>
          <li class="menu-has-children">
            <a href="#">Login</a>
            <ul>
              <li><a href="login_vol.php">Volunteer</a></li>
              <li><a href="login_org.php">Organisasi</a></li>
            </ul>
          </li>
          <li class="menu-has-children ">
            <a href="#">Registrasi</a>
            <ul>
              <li><a href="registrasi_vol.php">Volunteer</a></li>
              <li><a href="registrasi_org.php">Organisasi</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->
