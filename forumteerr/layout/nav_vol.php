<!--==========================
    Header
  ============================-->
  <header id="header" class="header-fixed">
    <div class="container">

      <div id="logo" class="pull-left">
        <h1><a href="volunteer.php" class="scrollto">Forumteer - Volunteer</a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="#intro"><img src="img/logo.png" alt="" title=""></a> -->
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li><a href="volunteer.php">Home</a></li>
          <li><a href="volunteer.php?page=my_event">My Events</a></li>
          <li class="menu-has-children menu-active">
            <a><?php echo $_SESSION['nama'] ?></a>
            <ul>
              <li><a href="volunteer.php?page=edit_profile">Edit Profile</a></li>
              <li><a href="logout.php">Log Out</a></li>
            </ul>
          </li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->
