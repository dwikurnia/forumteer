<?php 
session_start(); 
include 'config/db.php';

include 'layout/header.php';
include 'layout/nav_fixed.php';

?>

<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
      
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Login Organisasi</h3>
              <small> Silahkan masukkan username dan password Anda!</small>

              <div class="container text-left">
              	<br>
              	<form method="POST" action="model/login_validation_org.php" >
              		
	              	<div class="form-group" >
					    <label for="username" >Username</label>
					    <input type="text" class="form-control" id="username"  placeholder="Masukkan username anda!" name="username" required>
					  </div>
	
					  <div class="form-group">
					    <label for="passwdord">Password</label>
					    <input type="password" class="form-control" id="passwdord" placeholder="Password" name="password" required>
					  </div>
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Login</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->

<?php 
include 'layout/footer.php';

 ?>