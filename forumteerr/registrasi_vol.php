<?php 
session_start(); 
include 'config/db.php';

include 'layout/header.php';
include 'layout/nav_fixed.php';

?>

<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
      
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Registrasi volunteer</h3>
              <small> Mohon isi data volunteer Anda dengan benar!</small>

              <div class="container text-left">
              	<br>
              	<form method="POST" action="model/insert_vol.php" >
              		<div class="form-group" >
					    <label for="nama" >Nama volunteer</label>
					    <input type="text" class="form-control" id="nama"  placeholder="Masukkan nama anda!" name="nama" required>
					  </div>
					  <div class="form-group" >
					    <label for="alamat" >Alamat volunteer</label>
					    <input type="text" class="form-control" id="alamat"  placeholder="Masukkan alamat anda!" name="alamat" required>
					  </div>
					  <div class="row">
					  	<div class="form-group col-md-6" >
					    <label for="no_telp" >Nomor Telepon Organisasi</label>
					    <input type="number" class="form-control" id="no_telp"  placeholder="Masukkan no telepon anda!" name="no_telp" required>
					  </div> 
					  <div class="form-group col-md-2" >
					    <label for="no_telp" >Jenis Kelamin</label>
					  <select class="form-control" id="sel1" name="jenis_kelamin" required>
					  	<option></option>
					    <option value="L">Pria</option>
					    <option value="P">Wanita</option>
					  </select>
					  </div>
					  <div class="form-group col-md-4">
						  <label for="pwd">Tanggal Lahir </label>
						  <input type="date" class="form-control" id="ttl" name="tanggal_lahir">
						</div>
					  </div>
					 
					   <div class="form-group" >
					    <label for="tentang" >Biografi volunteer</label>
					    <textarea type="text" class="form-control" id="biografi"  rows="4" placeholder="Masukkan biografi anda!" name="biografi" required> </textarea> 
					  </div>
	              	<div class="form-group" >
					    <label for="username" >Username</label>
					    <input type="text" class="form-control" id="username"  placeholder="Masukkan username anda!" name="username" required>
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email anda!" name="email" required>
					  </div>
					  <div class="form-group">
					    <label for="passwdord">Password</label>
					    <input type="password" class="form-control" id="passwdord" placeholder="Password" name="password" required>
					  </div>
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Submit</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->

<?php 
include 'layout/footer.php';

 ?>