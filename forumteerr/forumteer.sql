-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2019 at 01:19 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forumteer`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id_event` int(11) NOT NULL,
  `username_organisasi` varchar(25) NOT NULL,
  `nama_event` varchar(50) NOT NULL,
  `deskripsi_event` text NOT NULL,
  `tempat_event` varchar(50) NOT NULL,
  `waktu_event` date NOT NULL,
  `gambar` varchar(35) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id_event`, `username_organisasi`, `nama_event`, `deskripsi_event`, `tempat_event`, `waktu_event`, `gambar`) VALUES
(3, 'organisasi_a', 'PEDULI TSUNAMI ', 'Gempa bumi dan tsunami Sulawesi 2018 adalah peristiwa gempa bumi berkekuatan 7,4 Mw[2][7] diikuti dengan tsunami yang melanda pantai barat Pulau Sulawesi, Indonesia, bagian utara pada tanggal 28 September 2018, pukul 18.02 WITA. Pusat gempa berada di 26 km utara Donggala dan 80 km barat laut kota Palu[8] dengan kedalaman 10 km. Guncangan gempa bumi dirasakan di Kabupaten Donggala, Kota Palu, Kabupaten Parigi Moutong, Kabupaten Sigi, Kabupaten Poso, Kabupaten Tolitoli, Kabupaten Mamuju bahkan hingga Kota Samarinda, Kota Balikpapan, dan Kota Makassar. Gempa memicu tsunami hingga ketinggian 5 meter di Kota Palu', 'PALU', '2019-11-21', 'gempa.jpg'),
(7, 'organisasi_a', 'CHARITY PANTI ASOEHAN', 'Pendidikan Yatim dan Dhuafa\r\nAdalah program pemenuhan kebutuhan pendidikan baik formal maupun nonformal yang meliputi beberapa kebutuhan. diantaranya: perlengkapan seragam sekolah, alat tulis, biaya ekstrakurikuler dan sarana penunjang lainnya.\r\n\r\nPola pendidikan yang diberikan memadukan konsep diniyah, pengembangan potensi anak dan skill kemandirian yang di implementasikan di sekolah formal maupun di asrama tempat mereka tinggal.', 'BOJONGSOANG', '2019-11-22', 'adik.jpg'),
(8, 'organisasi_a', 'SEKOLAH TERTINGGAL', '   Menteri Pendidikan dan Kebudayaan Muhadjir Effendy kembali mengunjungi daerah terdepan, terluar, dan tertinggal (3T), yakni Kabupaten Sorong, Papua Barat dan kabupaten Pohuwato, Gorontalo. Kunjungan dilakukan Mendikbud selama dua hari pada akhir pekan, pada Sabtu (13/1/2018) hingga Minggu (14/1/2018) kemarin. Di Sorong, Mendibud melakukan sidak ke SMK Negeri 3 Sorong. Di sekolah yang menjadi salah satu program revitalisasi ini, Mendikbud mengecek berbagai peralatan laboratorium. Mendikbud juga menanyakan kemitraan SMK ini dengan dunia usaha dan dunia industri.\r\n\r\nArtikel ini telah tayang di Kompas.com dengan judul \"Mendikbud Sidak ke Sekolah di Daerah Tertinggal\", https://nasional.kompas.com/read/2018/01/15/14472511/mendikbud-sidak-ke-sekolah-di-daerah-tertinggal?page=all.\r\nPenulis : Ihsanuddin', 'Medan', '2019-12-21', 'sekolah.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `join_events`
--

CREATE TABLE `join_events` (
  `id_join` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `username_volunteer` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `join_events`
--

INSERT INTO `join_events` (`id_join`, `id_event`, `username_volunteer`) VALUES
(24, 3, 'a'),
(25, 3, 'volunteer_a');

-- --------------------------------------------------------

--
-- Table structure for table `organisasi`
--

CREATE TABLE `organisasi` (
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `email` varchar(25) NOT NULL,
  `tentang` text NOT NULL,
  `no_telp` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `organisasi`
--

INSERT INTO `organisasi` (`username`, `password`, `nama`, `alamat`, `email`, `tentang`, `no_telp`) VALUES
('organisasi_a', '12345', 'MOCCA INC', 'Bandung', 'organisasi_a@a.com', 'ORGANISASI DIBIDANG PEDULI SESAMA DAN TIDAK TAKUT MARABAHAYA DIMANAPUN KAMI BERADA ', '12345'),
('organisasi_b', 'organisasi_b', 'Organisasi B', 'Bandung', 'organisasi_b@b.com', 'Organisasi B ', '08123123222'),
('organisasi_c', 'organisasi_c', 'Organisasi C', 'Bandung', 'organisasi_c@c.com', 'Organisasi C ', '08123123232');

-- --------------------------------------------------------

--
-- Table structure for table `volunteer`
--

CREATE TABLE `volunteer` (
  `username` varchar(25) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `no_telp` varchar(13) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `biografi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `volunteer`
--

INSERT INTO `volunteer` (`username`, `password`, `email`, `nama`, `no_telp`, `jenis_kelamin`, `tanggal_lahir`, `alamat`, `biografi`) VALUES
('a', 'a', 'a@a.com', 'AAasdsad', '01323', 'L', '2019-11-14', 'AAA', ' asdsadsadasd'),
('ikarahmayanti', 'ikarahmayanti', 'ikara@gmail.com', 'ikarahmayanti', '123', 'P', '1998-12-21', 'sukabirus', ' qwer'),
('volunteer_a', 'volunteer_a', 'volunteera@volunteera.com', 'Voluneer A', '12312312', 'L', '2019-11-28', 'Bandung', ' Voluneer A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id_event`),
  ADD KEY `fk_events` (`username_organisasi`);

--
-- Indexes for table `join_events`
--
ALTER TABLE `join_events`
  ADD PRIMARY KEY (`id_join`),
  ADD KEY `fk_join_2` (`username_volunteer`),
  ADD KEY `fk_join_1` (`id_event`);

--
-- Indexes for table `organisasi`
--
ALTER TABLE `organisasi`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `volunteer`
--
ALTER TABLE `volunteer`
  ADD PRIMARY KEY (`username`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id_event` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `join_events`
--
ALTER TABLE `join_events`
  MODIFY `id_join` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `events`
--
ALTER TABLE `events`
  ADD CONSTRAINT `fk_events` FOREIGN KEY (`username_organisasi`) REFERENCES `organisasi` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `join_events`
--
ALTER TABLE `join_events`
  ADD CONSTRAINT `fk_join_1` FOREIGN KEY (`id_event`) REFERENCES `events` (`id_event`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_join_2` FOREIGN KEY (`username_volunteer`) REFERENCES `volunteer` (`username`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
