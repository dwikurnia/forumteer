<?php 
// home page untuk organisasi
session_start(); 
// kalo gak ada sesi kemudian dilempar ke login
if ((!$_SESSION['username_vol'])){
  echo "<meta http-equiv=refresh content=\"0;url=index.php\">";
  exit;
}

include 'config/db.php';

include 'layout/header.php';
include 'layout/nav_vol.php';

include 'controller/c_volunteer.php';

include 'layout/footer.php';

 ?>