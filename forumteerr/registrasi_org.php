<?php 
session_start(); 
include 'config/db.php';

include 'layout/header.php';
include 'layout/nav_fixed.php';

?>

<section id="pricing" class="section-bg">
      <div class="container">
      	<br>
      
          <div class="col-lg-12" >
            <div class="box featured wow fadeInUp">
              <h3>Registrasi Organisasi</h3>
              <small> Mohon isi data organisasi Anda dengan benar!</small>

              <div class="container text-left">
              	<br>
              	<form method="POST" action="model/insert_org.php" >
              		<div class="form-group">
					    <label for="nama" >Nama Organisasi</label>
					    <input type="text" class="form-control" id="nama"  placeholder="Masukkan nama organisasi anda!" name="nama" required>
					  </div>
					  <div class="form-group" >
					    <label for="alamat" >Alamat Organisasi</label>
					    <input type="text" class="form-control" id="alamat"  placeholder="Masukkan alamat organisasi anda!" name="alamat" required>
					  </div>
					  <div class="form-group" >
					    <label for="no_telp" >Nomor Telepon Organisasi</label>
					    <input type="number" class="form-control" id="no_telp"  placeholder="Masukkan no telepon organisasi anda!" name="no_telp" required>
					  </div>
					   <div class="form-group" >
					    <label for="tentang" >Tentang Organisasi</label>
					    <textarea type="text" class="form-control" id="tentang"  rows="4" placeholder="Masukkan tentang organisasi anda!" name="tentang" required> </textarea> 
					  </div>
	              	<div class="form-group" >
					    <label for="username" >Username</label>
					    <input type="text" class="form-control" id="username"  placeholder="Masukkan username anda!" name="username" required>
					  </div>
					  <div class="form-group">
					    <label for="exampleInputEmail1">Email</label>
					    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Masukkan alamat email anda!" name="email" required>
					  </div>
					  <div class="form-group">
					    <label for="passwdord">Password</label>
					    <input type="password" class="form-control" id="passwdord" placeholder="Password" name="password" required>
					  </div>
					  <div class="text-center">
				  <button type="submit" class="btn get-started-btn ">Submit</button></div>
				</form>

              </div>
          
            </div>
          </div>

         
      </div>
    </section><!-- #pricing -->

<?php 
include 'layout/footer.php';

 ?>