<?php 
$path = './view/organisasi/';

if(isset($_GET['page'])){
    if ($_GET['page'] == "edit_profile") {
        include $path.'edit_org.php';
    } else if ($_GET['page'] == 'create_event'){
        include $path.'create_event.php';
    } else if ($_GET['page'] == 'edit_event'){
        include $path.'edit_event.php';
    } else if ($_GET['page'] == 'delete_event'){
        include './model/delete_event.php';
    } else if ($_GET['page'] == 'lihat_pendaftar'){
        include $path.'lihat_pendaftar.php';
    }


}else {
    // default org
    include $path.'content_org.php';
}

?>
