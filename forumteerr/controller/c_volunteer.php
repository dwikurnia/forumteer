<?php 
$path = './view/volunteer/';

if(isset($_GET['page'])){
    if ($_GET['page'] == "edit_profile") {
        include $path.'edit_vol.php';
    } else if ($_GET['page'] == "join_event"){
    	include './model/join_event.php';
    } else if ($_GET['page'] == "my_event"){
    	include $path.'my_event.php';
    } else if ($_GET['page'] == 'cancel_event'){
    	include './model/delete_join_event.php';
    }
}else {
    // default home
    include $path.'content_vol.php';
}

?>
